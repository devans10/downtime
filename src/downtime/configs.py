import os
import errno
import json

def get_configs(conf_file):

    if not os.path.isfile(conf_file):
        raise IOError(
                errno.ENOENT, os.strerror(errno.ENOENT), conf_file)

    with open(conf_file, 'r') as f:
        configs = json.load(f)

    return configs

def setup_config_file(conf_file):

    configs = {}
    configs['server'] = raw_input("Monitoring Server: ")
    configs['username'] = raw_input("Username: ")
    configs['password'] = raw_input("Password: ")

    with open(conf_file, 'w') as f:
        json.dump(configs, f)

