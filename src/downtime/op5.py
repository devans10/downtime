from __future__ import print_function
import ssl
import json
import socket
import base64
import urllib2
from datetime import datetime, timedelta


def call_downtime(confs):

    context = ssl.SSLContext(ssl.PROTOCOL_TLSv1)
    starttime = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    endtime = datetime.strftime(datetime.now() + timedelta(minutes = int(confs['duration'])), '%Y-%m-%d %H:%M:%S')
    hostname = socket.gethostname().split('.')[0]
    uri = 'https://%s/api/command/SCHEDULE_AND_PROPAGATE_HOST_DOWNTIME' % confs['server']
    headers = {"content-type": "application/json",
               "Authorization": "Basic %s" % (base64.b64encode('%s:%s' % (confs['username'], confs['password'])))}
    data = {"host_name": hostname,
            "start_time": starttime,
            "end_time": endtime,
            "fixed": "True",
            "trigger_id": "None",
            "duration": "2",
            "comment": confs['message']}

    request = urllib2.Request(uri, headers=headers)
    result = urllib2.urlopen(request, data=json.dumps(data), context=context)
    if result.getcode() != 200:
        raise Exception("HTTP Response: " + str(result.getcode()))
    else:
        return True

