from __future__ import print_function
from argparse import ArgumentParser

def create_parser():
    parser = ArgumentParser(description="""
    Place host in scheduled downtime in OP5
    """)
    parser.add_argument("--conf", "-c", help="Monitoring Configuration file")
    parser.add_argument("--message", "-m", required=True, help="Reason message for scheduled downtime")
    parser.add_argument("--duration", "-d", default="120", help="Duration of the downtime in minutes, default 120")
    parser.add_argument("--system", "-s", default="op5", help="Monitoring system, default op5")

    return parser

def main():
    import sys
    from downtime import configs

    args = create_parser().parse_args()

    if args.system.lower() == 'op5':
        from downtime import op5
        conf_file = '/root/.op5'

    try:
        confs = configs.get_configs(conf_file)
    except IOError:
        configs.setup_config_file(conf_file)
        confs = configs.get_configs(conf_file)

    confs['duration'] = args.duration
    confs['message'] = args.message

    if args.system.lower() == 'op5':
        try:
            op5.call_downtime(confs)
        except Exception as e:
            print("Error: Downtime Not Set. " + str(e))
            sys.exit(1)

        print("Downtime set for %s minutes." % args.duration)

