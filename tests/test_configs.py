import pytest

from downtime import configs

def test_config_file_not_found():
    """
    If get_configs raises error if config file does not exist
    """
    with pytest.raises(FileNotFoundError):
        configs.get_configs("/does/not/exist")
