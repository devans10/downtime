import pytest

from downtime import cli

@pytest.fixture
def parser():
    return cli.create_parser()

def test_parser_with_time(parser):
    """
    When passed the -m flag, duration equals value
    """
    args = parser.parse_args(["-m", "reboot", "-d", "30"])
    assert args.duration == "30"

def test_parser_without_time(parser):
    """
    With no args, duration equals default value
    """
    args = parser.parse_args(["-m", "reboot"])
    assert args.duration == "120"

def test_parser_without_message(parser):
    """
    Without message, exception is raised
    """
    with pytest.raises(SystemExit):
        parser.parse_args(["-d", "30"])

