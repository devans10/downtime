from setuptools import setup, find_packages

with open('README.rst') as f:
    readme = f.read()

setup(
    name='downtime',
    version='0.1.0',
    description='Commandline utility to place host in scheduled downtime in OP5',
    long_description=readme,
    author='Dave Evans',
    author_email='devans@duqlight.com',
    packages=['downtime'],
    package_dir={'': 'src'},
    entry_points={
        'console_scripts': [
            'downtime=downtime.cli:main',
        ],
    }
)
